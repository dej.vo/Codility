package pl.dejv.pluralsight.hackerrank;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Dawid Krężel on 17/09/16.
 */
public class Solution {

	private final int n;
	private final int[] arrayA;
	private final int[] arrayB;

	public Solution(int n, int[] tabA, int[] tabB) {
		this.n = n;
		this.arrayA = tabA;
		this.arrayB = tabB;
	}

	public int resolve() {

		int absResult = -1;
		ArrayList<Integer> values = new ArrayList<>();

		int counter = 0;
		for (int i = 0; i < n; i++) {
			int itemA = arrayA[i];

			int indexB = 0;
			for (int j = 0; j < n; j++) {
				if (itemA == arrayB[j]) {
					indexB = j;
				}
			}

			if (absResult == -1) {
				absResult = Math.abs(i - indexB);
				values.add(counter, itemA);
			} else if (Math.abs(i - indexB) < absResult) {
				absResult = Math.abs(i - indexB);
				values.remove(counter);
				values.add(counter, itemA);
			} else if (Math.abs(i - indexB) == absResult) {
				values.add(++counter, itemA);
			}
		}

		values.trimToSize();
		return Collections.min(values);
	}

}

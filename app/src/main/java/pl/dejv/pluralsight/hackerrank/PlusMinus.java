package pl.dejv.pluralsight.hackerrank;

/**
 * Created by Dawid Krężel on 18/09/16.
 */
public class PlusMinus {
	private final int n;
	private final int[] tab;
	private double positiveFraction = 0.0;
	private double negativeFraction = 0.0;
	private double zeroFraction = 0.0;

	public PlusMinus(int n, int[] tab) {
		this.n = n;
		this.tab = tab;
	}

	public void resolve() {
		int positiveCounter = 0;
		int negativeCounter = 0;
		int zeroCounter = 0;

		for (int i = 0; i < n; i++) {
			int item = tab[i];
			if (item > 0) {
				positiveCounter++;
			} else if (item < 0) {
				negativeCounter++;
			} else {
				zeroCounter++;
			}
		}

		positiveFraction = (double) positiveCounter / n;
		negativeFraction = (double) negativeCounter / n;
		zeroFraction = (double) zeroCounter / n;
	}

	public double getPositiveFraction() {
		return positiveFraction;
	}

	public double getNegativeFraction() {
		return negativeFraction;
	}

	public double getZerosFraction() {
		return zeroFraction;
	}
}

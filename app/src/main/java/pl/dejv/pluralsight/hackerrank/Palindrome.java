package pl.dejv.pluralsight.hackerrank;

/**
 * Created by dejv on 19/03/2017.
 */

public class Palindrome {

	public int check(final String s) {
		char[] tab = new char[128];
		for (int i = 0; i < s.length(); i++) {
//			int codePointAt = s.codePointAt(i);
			char charr = s.charAt(i);
			tab[charr]++;
		}
		int oddCounter = 0;
		for (int j = 0; j < tab.length; j++) {
			if (tab[j] % 2 != 0) {
				oddCounter++;
			}
		}
		return (oddCounter == 0 || oddCounter == 1) ? 1 : 0;
	}
}

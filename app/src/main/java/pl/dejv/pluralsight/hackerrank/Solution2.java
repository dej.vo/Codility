package pl.dejv.pluralsight.hackerrank;

import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * Created by Dawid Krężel on 18/09/16.
 */
public class Solution2 {
	private double[][] points3D;
	private boolean pointFound;

	public void resolve() {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();    // number of points
		int b = scanner.nextInt();    // bucket size

		int[] bucket = new int[b];

		HashMap<Double, Integer> indexZvalueMap = new HashMap<>();
		points3D = new double[n][3];
		for (int i = 0; i < n; i++) {
			int index = scanner.nextInt() - 1;
			points3D[index][0] = scanner.nextDouble();    // x
			points3D[index][1] = scanner.nextDouble();    // y
			points3D[index][2] = scanner.nextDouble();    // z
			indexZvalueMap.put(points3D[index][2], index);
		}

		TreeMap<Double, Integer> mainList = new TreeMap<>(indexZvalueMap);
		// replenish the bucket
		for (int i = 0; i < b; i++) {
			bucket[i] = mainList.pollLastEntry().getValue();
		}

		while (scanner.hasNext()) {
			String operation = scanner.next();
			int k = scanner.nextInt() - 1;

			pointFound = false;
			if (operation.equalsIgnoreCase("f")) {
				for (int j = 0; j < bucket.length; j++) {
					if (bucket[j] == k) {
						pointFound = true;
						break;
					}
				}
				if (pointFound) {
					printPoint(k);
				} else {
					System.out.println("Point doesn't exist in the bucket.");
				}

			} else if (operation.equalsIgnoreCase("r")) {
				for (int j = 0; j < bucket.length; j++) {
					if (bucket[j] == k) {
						pointFound = true;
						break;
					}
				}
				if (pointFound) {
					// remove point from the bucket and replace with new one
					if (mainList.isEmpty()) {
						System.out.println("No more points can be deleted.");
						return;
					}
					System.out.println("Point id " + k + " removed.");
					bucket[k] = mainList.pollLastEntry().getValue();
				} else {
					System.out.println("Point doesn't exist in the bucket.");
				}
			}
		}
	}

	public void printPoint(int index) {

		System.out.println(index + 1
				+ " = (" + String.format("%.3d", points3D[index][0])
				+ "," + String.format("%.3d", points3D[index][1])
				+ "," + String.format("%.3d", points3D[index][2])
				+ ")");
	}
}

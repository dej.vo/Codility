package pl.dejv.pluralsight.hackerrank;

/**
 * Created by Dawid Krężel on 18/09/16.
 */
public class DiagonalDifference {
	private final int n;
	private final int[][] matrix;

	public DiagonalDifference(int n, int[][] matrix) {
		this.n = n;
		this.matrix = matrix;
	}

	public int resolve() {
		int primaryDiagonal = 0;
		int secondaryDiagonal = 0;

		for (int i = 0; i < n; i++) {
			primaryDiagonal += matrix[i][i];
			secondaryDiagonal += matrix[i][n - 1 - i];
		}

		return Math.abs((primaryDiagonal - secondaryDiagonal));
	}
}

package pl.dejv.pluralsight.hackerrank;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by dejv on 19/03/2017.
 */

class CurrencyFormatter {
	private final double payment;

	public CurrencyFormatter(double payment) {
		this.payment = payment;
	}

	public String usaFormat() {
		NumberFormat currency = NumberFormat.getCurrencyInstance(Locale.US);
		return currency.format(payment);
	}

	public String chinaFormat() {
		NumberFormat currency = NumberFormat.getCurrencyInstance(Locale.CHINA);
		return currency.format(payment);
	}

	public String franceFormat() {
		NumberFormat currency = NumberFormat.getCurrencyInstance(Locale.FRANCE);
		return currency.format(payment);
	}

	public String indiaFormat() {
		Locale indiaLocale = new Locale("en", "IN");
		NumberFormat currency = NumberFormat.getCurrencyInstance(indiaLocale);
		return currency.format(payment);
	}
}

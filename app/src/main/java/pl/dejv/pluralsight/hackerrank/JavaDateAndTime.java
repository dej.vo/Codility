package pl.dejv.pluralsight.hackerrank;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by dejv on 19/03/2017.
 */

class JavaDateAndTime {

	public String getDayName(final String month, final String day, final String year) {
		Calendar cal = Calendar.getInstance();
		final int yearInt = Integer.parseInt(year);
		final int monthInt = Integer.parseInt(month);
		final int dayInt = Integer.parseInt(day);
		cal.set(yearInt, monthInt - 1, dayInt);

		return cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()).toUpperCase();
	}
}

package pl.dejv.pluralsight.hackerrank;

/**
 * Created by Dawid Krężel on 18/09/16.
 */
public class Staircase {
	private final int numberOfStairs;

	public Staircase(int numberOfStairs) {
		this.numberOfStairs = numberOfStairs;
	}

	public void print() {
		for (int i = 0; i < numberOfStairs; i++) {
			System.out.println(String.format("%" + (numberOfStairs) + "s", getStair(i + 1)));
		}
	}

	private String getStair(int i) {
		String stair = "";
		for (int j = 0; j < i; j++) {
			stair += "#";
		}
		return stair;
	}
}

package pl.dejv.pluralsight.hackerrank;

/**
 * Created by dejv on 18/03/2017.
 */

class CamelCase {

	public int countWords(String string) {
		if (string == null) {
			return -1;
		}
		string = string.trim();
		if (!string.isEmpty()) {
			int counter = 1;
			for (int i = 0; i < string.length(); i++) {
				char c = string.charAt(i);
				if (Character.isUpperCase(c)) {
					counter++;
				}
			}

			return counter;
		}
		return -1;
	}
}

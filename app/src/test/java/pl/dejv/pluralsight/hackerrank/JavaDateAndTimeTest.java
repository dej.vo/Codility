package pl.dejv.pluralsight.hackerrank;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

/**
 * Created by dejv on 19/03/2017.
 */

public class JavaDateAndTimeTest {

	private JavaDateAndTime javaDateAndTime;

	@Before
	public void init() {
		javaDateAndTime = new JavaDateAndTime();

	}

	@Test
	public void printDayTest() {
		//August 5th 2005
		String month = "08";    // AUGUST
		String day = "05";
		String year = "2015";
		String dayName = javaDateAndTime.getDayName(month, day, year);

		Calendar calendar = Calendar.getInstance();
		calendar.set(Integer.parseInt(year), Calendar.AUGUST, Integer.parseInt(day));

		String expectedDayName;
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

		switch (dayOfWeek) {
			case Calendar.MONDAY:
				expectedDayName = Day.MONDAY.getDayName();
				break;
			case Calendar.TUESDAY:
				expectedDayName = Day.TUESDAY.getDayName();
				break;
			case Calendar.WEDNESDAY:
				expectedDayName = Day.WEDNESDAY.getDayName();
				break;
			case Calendar.THURSDAY:
				expectedDayName = Day.THURSDAY.getDayName();
				break;
			case Calendar.FRIDAY:
				expectedDayName = Day.FRIDAY.getDayName();
				break;
			case Calendar.SATURDAY:
				expectedDayName = Day.SATURDAY.getDayName();
				break;
			case Calendar.SUNDAY:
				expectedDayName = Day.SUNDAY.getDayName();
				break;
			default:
				expectedDayName = "";
				break;
		}

		Assert.assertEquals(null, expectedDayName, dayName);
	}

	private enum Day {
		MONDAY(Calendar.MONDAY, "MONDAY"), TUESDAY(Calendar.TUESDAY, "TUESDAY"),
		WEDNESDAY(Calendar.WEDNESDAY, "WEDNESDAY"), THURSDAY(Calendar.THURSDAY, "THURSDAY"),
		FRIDAY(Calendar.FRIDAY, "FRIDAY"), SATURDAY(Calendar.SATURDAY, "SATURDAY"), SUNDAY(Calendar.SUNDAY, "SUNDAY");

		private final int dayInt;
		private final String dayName;

		Day(int dayInt, String dayName) {
			this.dayInt = dayInt;
			this.dayName = dayName;
		}

		public int getDayInt() {
			return dayInt;
		}

		public String getDayName() {
			return dayName;
		}

	}
}

package pl.dejv.pluralsight.hackerrank;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Dawid Krężel on 18/09/16.
 */
public class Solution2Test {

	private HashMap<Double, Integer> hashMap;

	@Before
	public void init() {
		//		4 2
		//		1 25.5 50.2 -60.5
		//		2 12.2 60.2 -75.89
		//		3 65.1 25.6 -55.9
		//		4 22.6 12.6 -30.8

		int n = 4;
		int b = 2;

		double[][] points3D = new double[][]{
				{25.5, 50.2, -60.5},
				{12.2, 60.2, -75.89},
				{65.1, 25.6, -55.9},
				{22.6, 12.6, -30.8}};

		hashMap = new HashMap<>();
		hashMap.put(-60.5, 1);
		hashMap.put(-75.89, 2);
		hashMap.put(-55.9, 3);
		hashMap.put(-30.8, 4);
	}

	@Test
	public void testResolve() {
		printMap(hashMap);

		TreeMap<Double, Integer> treeMap = new TreeMap<>(hashMap);
		printMap(treeMap);
		System.out.println(treeMap.pollLastEntry().getKey());
		System.out.println(treeMap.pollLastEntry().getKey());
	}

	private <K, V> void printMap(Map<K, V> map) {
		for (Map.Entry<K, V> entry : map.entrySet()) {
			System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
		}
	}

}
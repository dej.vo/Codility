package pl.dejv.pluralsight.hackerrank;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by Dawid Krężel on 18/09/16.
 */
public class StaircaseTest {

	private int numberOfStairs;

	@Before
	public void init() {
		numberOfStairs = 6;
	}

	@Test
	public void testResolve() {
		Staircase staircase = new Staircase(numberOfStairs);
		staircase.print();
	}
}

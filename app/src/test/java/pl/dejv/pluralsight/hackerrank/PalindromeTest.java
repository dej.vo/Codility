package pl.dejv.pluralsight.hackerrank;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

/**
 * Created by dejv on 19/03/2017.
 */
public class PalindromeTest {

	private Palindrome palindrome;

	@Before
	public void init() {
		palindrome = new Palindrome();
	}

	@Test
	public void testAnagramPalindrome1() {
		String input = "kayak";
		int result = palindrome.check(input);

		Assert.assertEquals(1, result);
	}

	@Test
	public void testAnagramPalindrome2() {
		String input = "dooernedeevrvn";
		int result = palindrome.check(input);

		Assert.assertEquals(1, result);
	}

	@Test
	public void testAnagramPalindrome3() {
		String input = "codilitytilidoc";
		int result = palindrome.check(input);

		Assert.assertEquals(1, result);
	}

	@Test
	public void testAnagramNotPalindrome1() {
		String input = "rocketboys";
		int result = palindrome.check(input);

		Assert.assertEquals(0, result);
	}

	@Test
	public void testAnagramNotPalindrome2() {
		String input = "codility";
		int result = palindrome.check(input);

		Assert.assertEquals(0, result);
	}

	@Test
	public void testComplexityN() {
		int n = 100000;
		StringBuilder strBuilder = new StringBuilder();
		for (char ch = 'a'; ch <= 'z'; ++ch) {
			strBuilder.append(ch);
		}
		Random random = new Random();
		char[] chars = strBuilder.toString().toCharArray();
		char[] bufor = new char[n];
		for (int i = 0; i < bufor.length; ++i) {
			bufor[i] = chars[random.nextInt(chars.length)];
		}
		String input = new String(bufor);
		int result = palindrome.check(input);

		Assert.assertEquals(0, result);
	}

	@Test
	public void testComplexity1() {
		int n = 1;
		StringBuilder strBuilder = new StringBuilder();
		for (char ch = 'a'; ch <= 'z'; ++ch) {
			strBuilder.append(ch);
		}
		Random random = new Random();
		char[] chars = strBuilder.toString().toCharArray();
		char[] bufor = new char[n];
		for (int i = 0; i < bufor.length; ++i) {
			bufor[i] = chars[random.nextInt(chars.length)];
		}
		String input = new String(bufor);
		int result = palindrome.check(input);

		Assert.assertEquals(1, result);
	}

}
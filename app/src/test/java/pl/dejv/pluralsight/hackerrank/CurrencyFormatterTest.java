package pl.dejv.pluralsight.hackerrank;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by dejv on 19/03/2017.
 */

public class CurrencyFormatterTest {

	private CurrencyFormatter currencyFormatter;

	@Before
	public void init() {
		final double payment = 12324.134;
		currencyFormatter = new CurrencyFormatter(payment);
	}

	@Test
	public void currencyFormatUSTest() {
		String expected = "$12,324.13";
		String formatted = currencyFormatter.usaFormat();

		Assert.assertEquals(expected, formatted);
	}

	@Test
	public void currencyFormatChinaTest() {
		String expected = "￥12,324.13";
		String formatted = currencyFormatter.chinaFormat();

		Assert.assertEquals(expected, formatted);
	}

	@Test
	public void currencyFormatFranceTest() {
		String expected = "12\u00A0324,13 €";        // \u00A0 - non-breaking space, char code 160, use this instead String.fromCharCode(160)
		String formatted = currencyFormatter.franceFormat();

		Assert.assertEquals(expected, formatted);
	}

	@Test
	public void currencyFormatIndiaTest() {
		String expected = "Rs.12,324.13";
		String formatted = currencyFormatter.indiaFormat();
		Assert.assertEquals(expected, formatted);
	}

}

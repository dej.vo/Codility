package pl.dejv.pluralsight.hackerrank;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {

	private Solution solution;

	@Before
	public void init() {
		int n = 5;
		int[] tabA = new int[]{1, 2, 3, 4, 5};
		int[] tabB = new int[]{5, 4, 3, 2, 1};
		solution = new Solution(n, tabA, tabB);
	}

	@Test
	public void addition_isCorrect() throws Exception {
		assertEquals(4, 2 + 2);
	}

	@Test
	public void testSolution() {
		int resolve = solution.resolve();
		assertEquals(3, resolve);
	}

}
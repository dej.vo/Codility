package pl.dejv.pluralsight.hackerrank;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Dawid Krężel on 18/09/16.
 */
public class PlusMinusTest {

	private int n;
	private int[] tab;

	//	6
	//	-4 3 -9 0 4 1
	@Before
	public void init() {
		n = 6;
		tab = new int[]{-4, 3, -9, 0, 4, 1};
	}

	@Test
	public void testResolve() {
		//		0.500000
		//		0.333333
		//		0.166667
		PlusMinus pm = new PlusMinus(n, tab);
		pm.resolve();

		Assert.assertEquals(0.500000, pm.getPositiveFraction(), 0.00001);
		Assert.assertEquals(0.333333, pm.getNegativeFraction(), 0.00001);
		Assert.assertEquals(0.166667, pm.getZerosFraction(), 0.00001);
	}
}

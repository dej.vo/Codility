package pl.dejv.pluralsight.hackerrank;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Dawid Krężel on 18/09/16.
 */
public class DiagonalDifferenceTest {

	private int n;
	private int[] row1;
	private int[] row2;
	private int[] row3;
	private int[][] matrix;

	//	3
	//	11 2 4
	//	4 5 6
	//	10 8 -12
	@Before
	public void init() {
		n = 3;
		row1 = new int[]{11, 2, 4};
		row2 = new int[]{4, 5, 6};
		row3 = new int[]{10, 8, -12};
		matrix = new int[][]{row1, row2, row3};
	}

	@Test
	public void testResult() {
		DiagonalDifference diagDiff = new DiagonalDifference(n, matrix);
		int result = diagDiff.resolve();

		Assert.assertEquals(15, result);
	}
}

package pl.dejv.pluralsight.hackerrank;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by dejv on 18/03/2017.
 */

public class CamelCaseTest {

	private CamelCase camelCase;

	@Before
	public void setup() {
		camelCase = new CamelCase();
	}

	@Test
	public void countWordsTest() {
		String input = "saveChangesInTheEditor";
		int n = camelCase.countWords(input);
		Assert.assertEquals(5, n);
	}

	@Test
	public void nullValueTest() {
		int n = camelCase.countWords(null);
		Assert.assertEquals(-1, n);
	}

	@Test
	public void whitespaceTest() {
		String input = "    ";
		int n = camelCase.countWords(input);
		Assert.assertEquals(-1, n);
	}

}
